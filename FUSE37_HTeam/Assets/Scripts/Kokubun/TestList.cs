﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TestList : MonoBehaviour
{
    LinkedList<List<GameObject>> m_UnitList = new LinkedList<List<GameObject>>();

    void FormationChange()
    {
        if (m_UnitList.Count < 2)
        {
            //部隊分割
            var unitNum = m_UnitList.First.Value.Count / 3.0f;
            var unitNumRemainder= m_UnitList.First.Value.Count % 3.0f;
            int samNum = 0;
            for(int i = 0; i < 3; i++)
            {
                m_UnitList.AddLast(new List<GameObject>());
                int num = 0;
                if (unitNumRemainder > 0)
                {
                    num = 1;
                    unitNumRemainder--;
                }
                for (int j = 0; j < unitNum + num; j++)
                {
                    m_UnitList.Last.Value.Add(m_UnitList.First.Value[samNum]);
                    samNum++;
                }
                m_UnitList.RemoveFirst();
            }
        }
        else
        {
            //部隊統合
            List<GameObject> saveList = new List<GameObject>();
            foreach(var units in m_UnitList)
            {
                foreach(var unit in units)
                {
                    saveList.Add(unit);
                }
            }
            m_UnitList.Clear();
            m_UnitList.AddLast(saveList);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
