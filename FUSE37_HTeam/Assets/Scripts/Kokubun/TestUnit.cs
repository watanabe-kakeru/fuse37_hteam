﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUnit : MonoBehaviour
{
    public bool flg = false;
    public GameObject m_UnitListObj;
    public int m_Key;

    public void SetKey(int key)
    {
        m_Key = key;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_UnitListObj = GameObject.Find("List");
    }

    // Update is called once per frame
    void Update()
    {
        if (flg)
        {
            var unitListObj=m_UnitListObj.GetComponent<UnitList>();
            unitListObj.DestroyUnit(m_Key);
            var unitList = unitListObj.GetUnitList();
            foreach(var units in unitList)
            {
                Debug.Log(units.Count);
                foreach(var unit in units)
                {
                    Debug.Log(unit.Key);
                }
            }
            Destroy(this.gameObject);
        }
    }
}
