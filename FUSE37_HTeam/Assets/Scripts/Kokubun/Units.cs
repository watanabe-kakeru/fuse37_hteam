﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units : MonoBehaviour
{
    //兵の配列が入る
    SortedList<int, GameObject> m_UnitList=new SortedList<int, GameObject>();
    //中心点からの相対位置
    Vector3 m_relativePosition = new Vector3(0,0,0);

    /// <summary>
    /// 兵の配列を貰う
    /// </summary>
    /// <param name="unitList"></param>
    public void SetUnitList(SortedList<int, GameObject> unitList)
    {
        m_UnitList = unitList;
        int count = 0;
        int tate = Mathf.FloorToInt(Mathf.Sqrt(m_UnitList.Count));
        Vector3 pos = new Vector3();
        foreach(var soldier in m_UnitList)
        {
            pos = new Vector3((tate/2) - (count / tate) ,0, (tate/2) - (count % tate));
            soldier.Value.GetComponent<Soldier>().SetRelativePosition(pos);
            count++;
        }
    }
    /// <summary>
    /// 兵の配列を貰う
    /// </summary>
    public void ResetUnitList()
    {
        m_UnitList = null;
    }
    /// <summary>
    /// 中心点からの相対位置に移動
    /// </summary>
    /// <param name="centerPoint">中心点の位置</param>
    public void Move(Vector3 centerPoint)
    {
        //中心点からの相対位置に移動する
        transform.position = new Vector3(centerPoint.x,0, centerPoint.z) + m_relativePosition;
        //兵たちも動かす
        foreach (var unit in m_UnitList)
        {
            unit.Value.GetComponent<Soldier>().Move(gameObject.transform.position);
        }
    }
    /// <summary>
    /// 相対位置を与える
    /// </summary>
    /// <param name="relativePositon">与える相対位置</param> 
    public void SetRelativePosition(Vector3 relativePositon)
    {
        m_relativePosition = relativePositon;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
