﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TestIntList : MonoBehaviour
{
    LinkedList<List<GameObject>> m_UnitList = new LinkedList<List<GameObject>>();

    public void FormationChange()
    {
        List<GameObject> saveList = new List<GameObject>();
        if (m_UnitList.Count < 2)
        {
            saveList = new List<GameObject>(m_UnitList.Last.Value.ToArray());
            m_UnitList.Clear();
            //部隊分割
            int unitNum = saveList.Count / 3;
            int unitNumRemainder = saveList.Count % 3;
            int samNum = 0;
            for (int i = 0; i < 3; i++)
            {
                int num = unitNum;
                if (unitNumRemainder > 0)
                {
                    num += 1;
                    unitNumRemainder--;
                }
                List<GameObject> addList = new List<GameObject>();
                for (int j = 0; j < num; j++)
                {
                    addList.Add(saveList[samNum]);
                    samNum++;
                }
                m_UnitList.AddLast(addList);
            }

            ////確認用
            //foreach (var unit in m_UnitList.Last.Value)
            //{
            //    Debug.Log(unit);
            //}
        }
        else
        {
            //部隊統合
            foreach (var units in m_UnitList)
            {
                Debug.Log(units.Count);
                foreach (var unit in units)
                {
                    saveList.Add(unit);
                }
            }
            m_UnitList.Clear();
            m_UnitList.AddLast(saveList);

            ////確認用
            //foreach (var unit in m_UnitList.First.Value)
            //{
            //    Debug.Log(unit);
            //}
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
