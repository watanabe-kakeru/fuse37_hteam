﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitList : MonoBehaviour
{
    //public bool TestSwitch = false;
    public Units[] units=new Units[3];
    //LinkedList<List<GameObject>> m_UnitList = new LinkedList<List<GameObject>>();
    LinkedList<SortedList<int, GameObject>> m_UnitList = new LinkedList<SortedList<int, GameObject>>();

    Vector3[] UnitRelativePosition = {
        new Vector3(4.0f,0,0),
        new Vector3(-2.0f,0,-2.0f),
        new Vector3(-2.0f,0,2.0f)
    };


    public LinkedList<SortedList<int, GameObject>> GetUnitList()
    {
        return m_UnitList;
    }

    public Units[] GetUnits()
    {
        return units;
    }

    //削除用テストコード
    public void DestroyUnit(int key)
    {
        foreach (var units in m_UnitList)
        {
            if (units.ContainsKey(key)){
                units.Remove(key);
            }
        }
    }

    public void FormationChange()
    {
        SortedList<int, GameObject> saveList = new SortedList<int, GameObject>();
        units[0].ResetUnitList();
        units[1].ResetUnitList();
        units[2].ResetUnitList();
        if (m_UnitList.Count < 2)
        {
            saveList = new SortedList<int, GameObject>(m_UnitList.Last.Value);
            m_UnitList.Clear();
            //部隊分割
            int unitNum = saveList.Count / 3;
            int unitNumRemainder = saveList.Count % 3;
            int samNum = 0;
            for (int i = 0; i < 3; i++)
            {
                int num = unitNum;
                if (unitNumRemainder > 0)
                {
                    num += 1;
                    unitNumRemainder--;
                }
                //兵の隊列を初期化
                SortedList<int, GameObject> addList = new SortedList<int, GameObject>();
                for (int j = 0; j < num; j++)
                {
                    //兵を追加する
                    addList.Add(saveList.Keys[samNum], saveList.Values[samNum]);
                    samNum++;
                }
                m_UnitList.AddLast(addList);
                //部隊に兵を渡す
                units[i].SetUnitList(addList);
                units[i].SetRelativePosition(UnitRelativePosition[i]);
            }

        }
        else
        {
            //部隊統合
            foreach (var units in m_UnitList)
            {
                foreach (var unit in units)
                {
                    saveList.Add(unit.Key, unit.Value);
                }
            }
            m_UnitList.Clear();
            m_UnitList.AddLast(saveList);
            //部隊に兵を渡す
            units[0].SetUnitList(saveList);
            units[0].SetRelativePosition(new Vector3(0, 0, 0));
            SortedList<int, GameObject> addList = new SortedList<int, GameObject>();
            units[1].SetUnitList(addList);
            units[2].SetUnitList(addList);
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }
}
