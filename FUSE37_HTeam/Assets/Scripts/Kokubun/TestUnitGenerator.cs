﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUnitGenerator : MonoBehaviour
{
    public GameObject m_UnitPrefab;
    public GameObject m_UnitListObj;
    // Start is called before the first frame update
    void Start()
    {
        var unitList = m_UnitListObj.GetComponent<UnitList>().GetUnitList();
        SortedList<int, GameObject> saveList = new SortedList<int, GameObject>();
        for (int i = 0; i < 10; i++)
        {
            var unit = Instantiate(m_UnitPrefab);
            unit.GetComponent<TestUnit>().SetKey(i);
            saveList.Add(i, unit);
        }
        unitList.AddLast(saveList);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
