﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int m_EnemyPower;
    public GameObject m_UnitListObj;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Soldier")
        {
            int unitPower=0;
            var key=other.GetComponent<Soldier>().m_Key;
            var unitList = m_UnitListObj.GetComponent<UnitList>().GetUnitList();
            foreach(var units in unitList)
            {
                if (units.ContainsKey(key))
                {
                    unitPower = units.Count;
                    break;
                }
            }
            if(unitPower> m_EnemyPower)
            {
                Destroy(gameObject);
            }
        }
    }
        // Start is called before the first frame update
        void Start()
    {
        //配列を管理するオブジェクトを探す
        m_UnitListObj = GameObject.Find("UnitList");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
