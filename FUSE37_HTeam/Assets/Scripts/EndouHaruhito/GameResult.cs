﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameResult : MonoBehaviour
{
    bool loadStandby = false;
    AudioSource audiSource;

    // Start is called before the first frame update
    void Start()
    {
        audiSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (loadStandby)
        {
            if (!audiSource.isPlaying)
            {
                //ここに　タイトル画面　の名前を入力してください
                SceneManager.LoadScene("TestTitle");
            }
        }
    }

    public void Result()
    {
        if (!loadStandby)
        {
            audiSource.Play();
            loadStandby = true;
        }
    }
}
