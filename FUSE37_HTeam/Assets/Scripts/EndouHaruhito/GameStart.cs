﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour
{
    bool startStandby = false;
    AudioSource audiSource;

    // Start is called before the first frame update
    void Start()
    {
        audiSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (Input.GetKeyDown("joystick button 0"))
        {
            ClickGameStart();
        }

        if (startStandby)
        {
            if (!audiSource.isPlaying)
            {
                //ここに　ゲーム画面　の名前を入力してください
                SceneManager.LoadScene("Battle");
            }
        }
    }

    public void ClickGameStart()
    {
        if (!startStandby)
        {
            audiSource.Play();
            startStandby = true;
        }
    }

}
