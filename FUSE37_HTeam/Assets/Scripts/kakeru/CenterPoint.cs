﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterPoint : MonoBehaviour
{
    //移動にかける速度
    float m_speed = 2.0f;
    //部隊と兵を管理するオブジェクト
    public GameObject m_UnitListObj;
    //兵のプレファブ
    public GameObject m_soldierPrefab;

    // Start is called before the first frame update
    void Start()
    {
        var unitList = m_UnitListObj.GetComponent<UnitList>().GetUnitList();
        //兵たちを生成
        SortedList<int, GameObject> saveList = new SortedList<int, GameObject>();
        for (int i = 0; i < 20; i++)
        {
            var soldier = Instantiate(m_soldierPrefab);
            soldier.GetComponent<Soldier>().SetKey(i);
            saveList.Add(i, soldier);
        }
        unitList.AddLast(saveList);
        m_UnitListObj.GetComponent<UnitList>().units[0].SetUnitList(saveList);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("joystick button 0") || Input.GetKeyDown(KeyCode.L))
        {
            m_UnitListObj.GetComponent<UnitList>().FormationChange();
        }
            Move();
    }

    /// <summary>
    /// スティックの入力で移動
    /// </summary>
    private void Move()
    {
        //処理を行う長さの基準
        const float executionLength = 0.2f;
        //スティックの各入力を保存
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        //ノーマライズ化して進行方向を保存
        Vector3 moveVec = new Vector3(horizontal, 0, vertical);
        moveVec = moveVec.normalized;
        //入力の長さが処理を行う長さを超えていたら処理
        if (moveVec.magnitude >= executionLength)
        {
            //位置を更新する
            Vector3 position = transform.position;
            position = position + (moveVec * m_speed * Time.deltaTime);
            transform.position = position;

            //部隊を持ってくる
            var units = m_UnitListObj.GetComponent<UnitList>().GetUnits();
            //部隊の位置更新をする
            foreach (var unit in units)
            {
                unit.Move(transform.position);
            }
        }
    }
}
