﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour
{
    //中心点からの相対位置
    Vector3 m_relativePosition = new Vector3(0,0,0);
    //部隊と兵を管理するオブジェクト
    public GameObject m_UnitListObj;
    //自身の配列内での番号キー
    public int m_Key;

    // Start is called before the first frame update
    void Start()
    {
        //配列を管理するオブジェクトを探す
        m_UnitListObj = GameObject.Find("UnitList");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// 中心点からの相対位置に移動する
    /// </summary>
    /// <param name="relativePositon">与える相対位置</param> 
    public void Move(Vector3 centerPoint)
    {
        //中心点からの相対位置に移動する
        transform.position = centerPoint + m_relativePosition;
    }
    /// <summary>
    /// 陣形変化時の移動
    /// </summary>
    private void FormationMove()
    {

    }

    /// <summary>
    /// 死亡判定
    /// </summary>
    public void Death()
    {
        //配列から自身を取り除く
        var unitListObj = m_UnitListObj.GetComponent<UnitList>();
        unitListObj.DestroyUnit(m_Key);
        //ゲームから消える
        Destroy(this.gameObject);
    }

    /// <summary>
    /// 相対位置を与える
    /// </summary>
    /// <param name="relativePositon">与える相対位置</param> 
    public void SetRelativePosition(Vector3 relativePositon)
    {
        m_relativePosition = relativePositon;
    }
    /// <summary>
    /// 管理オブジェクトが兵を生成したときに呼ぶ
    /// </summary>
    /// <param name="key">配列内での番号キー</param>
    public void SetKey(int key)
    {
        m_Key = key;
    }
}
